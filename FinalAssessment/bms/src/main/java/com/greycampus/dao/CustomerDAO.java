package com.greycampus.dao;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.greycampus.entity.AustraliaStateEntity;
import com.greycampus.entity.CanadaStateEntity;
import com.greycampus.entity.CountryEntity;
import com.greycampus.entity.CustomerEntity;
import com.greycampus.entity.IndiaStateEntity;
import com.greycampus.entity.UsaStateEntity;
import com.greycampus.projectcustomer.AustraliaStateRepo;
import com.greycampus.projectcustomer.CanadaStateRepo;
import com.greycampus.projectcustomer.CountryRepo;
import com.greycampus.projectcustomer.CustomerRepository;
import com.greycampus.projectcustomer.IndiaStateRepo;
import com.greycampus.projectcustomer.UsaStateRepo;

@Repository
public class CustomerDAO {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CountryRepo countryRepo;
//
//	@Autowired
//	private StateRepo stateRepo;

	@Autowired
	private AustraliaStateRepo australiaStateRepo;

	@Autowired
	private IndiaStateRepo indiaStateRepo;

	@Autowired
	private UsaStateRepo usaStateRepo;

	@Autowired
	private CanadaStateRepo canadaStateRepo;

	@Transactional
	public ArrayList<UsaStateEntity> getUsaStates() {
		return usaStateRepo.findAll();
		 
	}
	
	@Transactional
	public ArrayList<IndiaStateEntity> getIndiaStates() {
		return indiaStateRepo.findAll();
	}
	
	@Transactional
	public ArrayList<CanadaStateEntity> getCanadaStates() {
		return canadaStateRepo.findAll();
	}
	
	@Transactional
	public ArrayList<AustraliaStateEntity> getAustraliaStates() {
		return australiaStateRepo.findAll();
	}

		
	
	@Transactional
	public void addCustomer(CustomerEntity customer) {
		customerRepository.save(customer);

	}

	@Transactional
	public void removeCustomer(CustomerEntity customer) {
		customerRepository.delete(customer);
	}

	@Transactional
	public ArrayList<CustomerEntity> searchCustomerById(Integer id) {

		return customerRepository.findByCustomerId(id);
	}

	@Transactional
	public CustomerEntity searchbycust(Integer id) {
		CustomerEntity entity = customerRepository.findByCustId(id);
		return entity;

	}

	@Transactional
	public ArrayList<CustomerEntity> searchCustomer() {

		return customerRepository.findAll();

	}

	@Transactional
	public void updateCustomer(CustomerEntity customer) {
		customerRepository.save(customer);
	}

	@Transactional
	public ArrayList<CountryEntity> getCountry() {
		ArrayList<CountryEntity> employeeList = countryRepo.findAll();
		return employeeList;
	}

//	@Transactional
//	public ArrayList<StateEntity> getState() {
//		return stateRepo.findAll();
//	}

}
package com.greycampus.services;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greycampus.dao.CustomerDAO;
import com.greycampus.entity.AustraliaStateEntity;
import com.greycampus.entity.CanadaStateEntity;
import com.greycampus.entity.CountryEntity;
import com.greycampus.entity.CustomerEntity;
import com.greycampus.entity.IndiaStateEntity;
import com.greycampus.entity.UsaStateEntity;

@Service
public class CustomerServices {

	@Autowired
	private CustomerDAO customerDAO;

	public void addCustomer(CustomerEntity customer) {
		customerDAO.addCustomer(customer);
	}

	public void updateCustomer(CustomerEntity customer) {
		customerDAO.updateCustomer(customer);
	}

	public void removeCustomer(CustomerEntity customer) {
		customerDAO.removeCustomer(customer);
	}

	public ArrayList<CustomerEntity> searchCustomerById(Integer id) {

		return customerDAO.searchCustomerById(id);
	}

	public ArrayList<CustomerEntity> searchCustomer() {

		return customerDAO.searchCustomer();

	}

	public CustomerEntity searchbycust(Integer id) {
		CustomerEntity entity = customerDAO.searchbycust(id);
		return entity;

	}

	// To Get The COuntry
	public ArrayList<CountryEntity> getCountry() {
		return customerDAO.getCountry();

	}

	// To Get The States

	@Transactional
	public ArrayList<UsaStateEntity> getUsaStates() {
		return customerDAO.getUsaStates();

	}

	@Transactional
	public ArrayList<IndiaStateEntity> getIndiaStates() {
		return customerDAO.getIndiaStates();
	}

	@Transactional
	public ArrayList<CanadaStateEntity> getCanadaStates() {
		return customerDAO.getCanadaStates();
	}

	@Transactional
	public ArrayList<AustraliaStateEntity> getAustraliaStates() {
		return customerDAO.getAustraliaStates();
	}

}
package com.greycampus.projectcustomer;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import com.greycampus.entity.CanadaStateEntity;

public interface CanadaStateRepo extends CrudRepository<CanadaStateEntity, Integer>{
	public ArrayList<CanadaStateEntity> findAll();

}